<?php

namespace Domain\Repositories;

use Doctrine\Common\Persistence\ObjectRepository;

Interface UserRepository extends ObjectRepository {

}