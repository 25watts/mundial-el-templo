<?php

namespace Domain\Repositories;

use Doctrine\Common\Persistence\ObjectRepository;

Interface ContactRepository extends ObjectRepository {

}