<?php

namespace Neverland\Console\Commands;

use Illuminate\Console\Command;

class CreateRepository extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:repository {repository}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a repository for specific Entity';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $repository = $this->argument('repository');

        $this->createInterface($repository);

        $this->createInfrastructure($repository);

        $this->editServiceProvider($repository);

    }

    private function editServiceProvider($repository)
    {





    }

    /**
     * @param $repository
     */
    private function createInfrastructure($repository)
    {
        chdir('../../app/Infrastructure');

        $file = fopen('Doctrine' . $repository . '.php', 'w');

        $txt = "<?php";
        fwrite($file, $txt);
        $txt = "\n\n";
        fwrite($file, $txt);
        $txt = "namespace ".env('APP_NAME')."\Infrastructure;";
        fwrite($file, $txt);
        $txt = "\n\n";
        fwrite($file, $txt);
        $txt = "use Domain\Repositories\\{$repository};";
        fwrite($file, $txt);
        $txt = "\n\n";
        fwrite($file, $txt);
        $txt = "Class Doctrine{$repository} extends DoctrineCoreRepository implements {$repository} {";
        fwrite($file, $txt);
        $txt = "\n\n";
        fwrite($file, $txt);
        $txt = "}";
        fwrite($file, $txt);

        fclose($file);
    }

    /**
     * @param $repository
     */
    private function createInterface($repository)
    {
        chdir('domain');

        if ( ! file_exists('Repositories'))
            mkdir('Repositories');

        chdir('Repositories');


        //file_put_contents($repository . 'php');

        $file = fopen($repository . '.php', 'w');

        $txt = "<?php";
        fwrite($file, $txt);
        $txt = "\n\n";
        fwrite($file, $txt);
        $txt = "namespace Domain\Repositories;";
        fwrite($file, $txt);
        $txt = "\n\n";
        fwrite($file, $txt);
        $txt = "use Doctrine\Common\Persistence\ObjectRepository;";
        fwrite($file, $txt);
        $txt = "\n\n";
        fwrite($file, $txt);
        $txt = "Interface {$repository} extends ObjectRepository {";
        fwrite($file, $txt);
        $txt = "\n\n";
        fwrite($file, $txt);
        $txt = "}";
        fwrite($file, $txt);

        fclose($file);
    }
}
