<?php

namespace Neverland\Providers;

use Domain\Contact;
use Domain\Repositories\ContactRepository;
use Domain\Repositories\UserRepository;
use Domain\User;
use Illuminate\Support\ServiceProvider;
use Neverland\Infrastructure\DoctrineContactRepository;
use Neverland\Infrastructure\DoctrineUserRepository;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {

        #start
        $this->app->bind(UserRepository::class, function($app) {
            // This is what Doctrine's EntityRepository needs in its constructor.
            return new DoctrineUserRepository(
                $app['em'],
                $app['em']->getClassMetaData(User::class)
            );
        });
        $this->app->bind(ContactRepository::class, function($app) {
            // This is what Doctrine's EntityRepository needs in its constructor.
            return new DoctrineContactRepository(
                $app['em'],
                $app['em']->getClassMetaData(Contact::class)
            );
        });
        #end
    }
}