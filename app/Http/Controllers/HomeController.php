<?php

namespace Neverland\Http\Controllers;

use Doctrine\ORM\EntityManager;
use Domain\Contact;
use Faker\Provider\Image;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('landing');
    }

    /**
     * @param Request $request
     */
    public function contact_store(Request $request)
    {
        /*$contact = new Contact();
        $contact->setFirstName($request->firstname); //firstname
        $contact->setLastName($request->lastname); //lastname
        $contact->setEmail($request->email); //email
        $contact->setPhone($request->phone); //phone
        $contact->setCity($request->city); //city
        $contact->setFoundOut($request->howto); //howto
        $contact->setOftenTime($request->visitoften); //visitoften
        $contact->setPack($request->pack); //pack

        \LaravelDoctrine\ORM\Facades\EntityManager::persist($contact);
        \LaravelDoctrine\ORM\Facades\EntityManager::flush();

        return view('thanks');*/

        return new JsonResponse([
            'hello' => 'world'
        ]);
    }

    public function image_store(Request $request)
    {
        $image_base64 = $request->get('image');

        $file_name = 'image_'.time().'.svg';

        @list($type, $image_base64) = explode(';', $image_base64);
        @list(, $image_base64) = explode(',', $image_base64);

        if($image_base64 != ""){ // storing image in storage/app/public Folder
            Storage::disk('public')->put($file_name,base64_decode($image_base64));
        }

        return new JsonResponse([
            'path' => url('storage/' . $file_name)
        ]);
    }

    public function save(Request $request)
    {

    }
}
