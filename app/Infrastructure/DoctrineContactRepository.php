<?php

namespace Neverland\Infrastructure;

use Domain\Repositories\ContactRepository;
use LaravelDoctrine\ORM\Pagination\PaginatesFromRequest;

Class DoctrineContactRepository extends DoctrineCoreRepository implements ContactRepository {

    use PaginatesFromRequest;

}