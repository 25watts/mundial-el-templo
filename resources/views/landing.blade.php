<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <meta property="description" name="description" content="Notas focalizadas en el quehacer político de la Argentina.">
    <meta property="og:app_id" name="og:app_id" content="2060866897565447">
    <meta property="og:locale" name="og:locale" content="es_AR">
    <meta property="og:title" name="og:title" content="El Templo | La otra mirada">
    <meta property="og:site_name" name="og:site_name" content="Mundial">
    <meta property="og:type" name="og:type" content="website">
    <meta property="og:url" name="og:url" content="https://mundial.local.25watts.com.ar/">
    <meta property="og:description" name="og:description" content="Notas focalizadas en el quehacer político de la Argentina.">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">

    <script src="https://code.jquery.com/jquery-3.2.1.min.js" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/svg.js/2.6.5/svg.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/canvg/1.5/canvg.min.js"></script>

    <title>Document</title>
</head>

<body>

    <div id="cancha">
        <div id="chose" class="modal chose">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Modal title</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">

                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#arqueros">Arqueros</a></li>
                            <li><a data-toggle="tab" href="#defensores">Defensores</a></li>
                            <li><a data-toggle="tab" href="#volantes">Volantes</a></li>
                            <li><a data-toggle="tab" href="#delanteros">Delanteros</a></li>
                        </ul>
                            
                        <div class="tab-content">
                            <div id="arqueros" class="tab-pane fade in active">
                                <ul>
                                    <li><a href="#" class="chose-player" data-name="Armani">Armani</a></li>
                                    <li><a href="#" class="chose-player" data-name="Caballero">Caballero</a></li>
                                    <li><a href="#" class="chose-player" data-name="Romero">Romero</a></li>
                                </ul>
                            </div>
                            <div id="defensores" class="tab-pane fade">
                                <ul>
                                    <li><a href="#" class="chose-player" data-name="Mercado">Mercado</a></li>
                                    <li><a href="#" class="chose-player" data-name="Ansaldi">Ansaldi</a></li>
                                    <li><a href="#" class="chose-player" data-name="Otamendi">Otamendi</a></li>
                                    <li><a href="#" class="chose-player" data-name="Fazio">Fazio</a></li>
                                    <li><a href="#" class="chose-player" data-name="Rojo">Rojo</a></li>
                                    <li><a href="#" class="chose-player" data-name="Tagliafico">Tagliafico</a></li>
                                    <li><a href="#" class="chose-player" data-name="Acunia">Acunia</a></li>
                                </ul>
                            </div>
                            <div id="volantes" class="tab-pane fade">
                                <ul>
                                    <li><a href="#" class="chose-player" data-name="Mascherano">Mascherano</a></li>
                                    <li><a href="#" class="chose-player" data-name="Salvio">Salvio</a></li>
                                    <li><a href="#" class="chose-player" data-name="Biglia">Biglia</a></li>
                                    <li><a href="#" class="chose-player" data-name="Lo Celso">Lo Celso</a></li>
                                    <li><a href="#" class="chose-player" data-name="Banega">Banega</a></li>
                                    <li><a href="#" class="chose-player" data-name="Lanzini">Lanzini</a></li>
                                    <li><a href="#" class="chose-player" data-name="Meza">Meza</a></li>
                                    <li><a href="#" class="chose-player" data-name="Di Maria">Di Maria</a></li>
                                    <li><a href="#" class="chose-player" data-name="Pavon">Pavon</a></li>
                                </ul>
                            </div>
                            <div id="delanteros" class="tab-pane fade">
                                <ul>
                                    <li><a href="#" class="chose-player" data-name="Messi">Messi</a></li>
                                    <li><a href="#" class="chose-player" data-name="Dybala">Dybala</a></li>
                                    <li><a href="#" class="chose-player" data-name="Higuain">Higuain</a></li>
                                    <li><a href="#" class="chose-player" data-name="Aguero">Aguero</a></li>                                    
                                </ul>
                            </div>
                        </div>

                        
                    </div>
                    <div class="modal-footer">
                        
                    </div>
                </div>
            </div>            
        </div>

        <div class="container">
            <div class="row">
                <div class="col-xs-6">
                    <a href="" id="firstPosition">4-3-3</a>

                    <a href="" id="secondPosition">4-3-1-2</a>

                    <a href="" id="thirdPosition">4-2-3-1</a>

                    <a href="" id="fourPosition">5-3-2</a>
                </div>
                <div class="col-xs-6 text-right hidden" id="share">
                    <a href="#">Compartir Facebook</a>
                </div>
            </div>
        </div>
    </div>

    <script src="{{ asset('js/front/positions.js') }}"></script>
    <script src="{{ asset('js/front/app.js') }}"></script>

    <script>

            $(function () {
                //$('#share').hide();
            });

            window.fbAsyncInit = function() {

                FB.init({
                    appId      : 2060866897565447,
                    cookie     : true,
                    xfbml      : true,
                    version    : 'v3.0'
                });

                FB.AppEvents.logPageView();

                FB.getLoginStatus(function(response) {
                    console.log(response);
                    //statusChangeCallback(response);
                });

                $('#share').on('click', function () {

                    FB.login(function(response) {
                        if (response.authResponse) {

                            let svg = document.getElementById("SvgjsSvg1006");
                            let svgData = new XMLSerializer().serializeToString( svg );

                            let canvas = document.createElement( "canvas" );
                            let ctx = canvas.getContext( "2d" );

                            let img = document.createElement( "img" );
                            img.setAttribute( "src", "data:image/svg+xml;base64," + btoa( svgData ) );

                            ctx.drawImage( img, 0, 0 );

                            img.onload = function() {

                                ///console.log(canvas.toDataURL( "image/png" ));
                                //console.log($(img).attr('src'));

                                $.post("{{ route('image.store') }}", {
                                    '_token': "{{ csrf_token() }}",
                                    'image': $(img).attr('src')
                                }, function (data) {

                                    $('head').append( '<meta name="og:image" content="' + data.path + '">' );

                                    FB.ui({
                                        method: 'share',
                                        href: 'https://mundial.local.25watts.com.ar/',
                                    }, function(response){
                                        console.log(response);
                                    });
                                });
                            };

                        } else {
                            console.log("user did not give permission");
                        }
                    }, {scope:'email'});

                });

            };

            (function(d, s, id){
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) {return;}
                js = d.createElement(s); js.id = id;
                js.src = "https://connect.facebook.net/en_US/sdk.js";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));

    </script>

</body>
</html>