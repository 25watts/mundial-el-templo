
$(document).ready(function(){

  // -------------------------------------------- Navbar on scroll -------------------------------------------- //
  var $menuNav = $('#navMain');
  var menuNavHeight = $menuNav.outerHeight();
  var useFixedMenu = $(document).scrollTop() > menuNavHeight;

  // NAVBAR MOBILE
  $menuNav.toggleClass('navbar-mobile', useFixedMenu);
  $(document).scroll(function() {
      useFixedMenu = $(document).scrollTop() > menuNavHeight;
      $menuNav.toggleClass('navbar-mobile', useFixedMenu);
  });
  $('#navbar-toggle').click(function(){
    $(this).toggleClass('hamburger-animation');
  });
  // -------------------------------------------- Navbar on scroll -------------------------------------------- //


  // -------------------------------------------- Parallax -------------------------------------------- //
  // $.stellar();
  // -------------------------------------------- Parallax -------------------------------------------- //


  // -------------------------------------------- Animations -------------------------------------------- //
  // new WOW().init();
  // -------------------------------------------- Animations -------------------------------------------- //


  // -------------------------------------------- Stop Video On close modal -------------------------------------------- //
	// $('#myModal_video').on('hidden.bs.modal', function (e) {
  //   var stopVideo = function(player) {
  //     var vidSrc = player.prop('src');
  //     player.prop('src', ''); // to force it to pause
  //     player.prop('src', vidSrc);
  //   };
  //   stopVideo($('#videoyt'));
  // });
  // -------------------------------------------- Stop Video On close modal -------------------------------------------- //

  // -------------------------------------------- Smooth Scroll -------------------------------------------- //
  $('.smooth').click(function(){
    $('html, body').animate({
      scrollTop: $( $.attr(this, 'href') ).offset().top - menuNavHeight
    }, 600);
    return false;
  });

  $('.back-to-top').click(function(){
    $("html, body").animate({ scrollTop: 0 }, 600);
      return false;
  });
  // -------------------------------------------- Smooth Scroll -------------------------------------------- //

  // -------------------------------------------- Form without ajax -------------------------------------------- //
    var $form = $(".form-validate");
  
    if ($form.length) {
        $form.each(function(){
            var $submit = $('.submit', this);
            var $formSending = $('.form-sending', this);
  
            $(this).validate({
                onfocusout: false,
                onkeyup: false,
                onclick: false,
                errorElement: "div",
                errorClass: 'errorField'
            });
        });
    }
  // -------------------------------------------- Form without ajax -------------------------------------------- //


  // -------------------------------------------- Owl Carousel -------------------------------------------- //
    $('.owl-carousel').owlCarousel();
  // -------------------------------------------- Owl Carousel -------------------------------------------- //

});
